export const DARK_GRADIENT = '#702657';
export const LIGHT_GRADIENT = '#a40a14';
export const EMPTY_CUP = '#d2c5ba';
export const WATER = '#63aec5';
export const MILK = '#b7a080';
export const COFFEE = '#2c0d08';