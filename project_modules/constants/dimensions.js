import {Dimensions, PixelRatio} from 'react-native';
//WIDTH AND HEIGHT
export const { width, height } = Dimensions.get('window');
export const coefficientWidth = width / 300;
export const coefficientHeight = height / 300;

//FONT SIZES
export const SIZE_H1 = 35 * coefficientWidth;
export const SIZE_H2 = 20 * coefficientWidth;
export const SIZE_H3 = 18 * coefficientWidth;
export const SIZE_H4 = 16 * coefficientWidth;
export const SIZE_H5 = 14 * coefficientWidth;
export const SIZE_H6 = 12 * coefficientWidth;
export const SIZE_H7 = 10 * coefficientWidth;

export const CUP_SIZE = {
    110: 0.13846,
    175: 0.15753,
    200: 0.11364,
    250: 0.13587,
    400: 0.14545,
    500: 0.11765
};