import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Button, Icon} from 'native-base'
import {DARK_GRADIENT} from "../constants/colors";

class RoundButton extends React.Component {
    render() {
        return (
            <Button light rounded disabled={false} onPress={() => this.props.press()}
                    style={[styles.content, this.props.style]}>
                <Icon style={{fontSize: 30, color: DARK_GRADIENT}} name={this.props.name}/>
            </Button>
        )
    }
}

RoundButton.defaultProps = {
    name: 'close',
    style: {},
    press: () => 1
};

const styles = StyleSheet.create({
    content: {
        height: 50,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
});

export default RoundButton