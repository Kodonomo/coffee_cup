import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View} from 'react-native';
import {CUP_SIZE} from "../constants/dimensions";

class Trapezoid extends React.Component {
    render() {
        let tanAngle = CUP_SIZE["175"];
        let side = Math.round(this.props.height * tanAngle);
        let style = {
            width: this.props.width,
            borderBottomWidth: this.props.height,
            borderBottomColor: this.props.color,
            borderLeftWidth: side,
            borderRightWidth: side,
        };
        let innerStyle = {
            width: this.props.width - side * 2,
            height: this.props.height
        };

        return (
            <View style={[styles.content, style]}>
                <View style={[styles.child, innerStyle]}>
                    {this.props.children}
                </View>
            </View>
        )
    }
}

Trapezoid.defaultProps = {
    width: 10,
    height: 5,
    color: 'red'
};

Trapezoid.propTypes = {
    children: PropTypes.element.isRequired
};

const styles = StyleSheet.create({
    content: {
        height: 0,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderStyle: 'solid',
        transform: [
            {rotate: '180deg'}
        ]
    },
    child: {
        alignItems: 'center',
        justifyContent: 'center',
        transform: [
            {rotate: '180deg'}
        ]
    }
});

export default Trapezoid