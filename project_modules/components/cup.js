import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, Text, View} from 'react-native';
import Trapezoid from "./trapezoid";
import {coefficientHeight, coefficientWidth, CUP_SIZE, SIZE_H6, SIZE_H7} from "../constants/dimensions";
import {COFFEE, EMPTY_CUP, MILK, WATER} from "../constants/colors";

class Cup extends React.Component {
    render() {
        let bottomWidth = 140 * coefficientWidth;
        let fullHeight = 150 * coefficientHeight;
        let {
            coffeeHeight,
            waterHeight,
            milkHeight,
            percent
        } = this.props;
        if (percent) {
            coffeeHeight *= fullHeight;
            waterHeight *= fullHeight;
            milkHeight *= fullHeight;
        }
        let sum = coffeeHeight + waterHeight + milkHeight;
        let tanAngle = CUP_SIZE["175"];
        let coffeeWidth = (coffeeHeight) * tanAngle * 2;
        let waterWidth = (waterHeight) * tanAngle * 2 + coffeeWidth;
        let milkWidth = (milkHeight) * tanAngle * 2 + waterWidth;
        let emptyWidth = (fullHeight - sum) * tanAngle * 2 + milkWidth;

        return (
            <View style={[styles.container, this.props.style]}>
                {(fullHeight - sum) && <Trapezoid width={bottomWidth + emptyWidth}
                           height={fullHeight - sum} color={EMPTY_CUP}>
                    <Text/>
                </Trapezoid>}
                {!!milkHeight && <Trapezoid width={bottomWidth + milkWidth} height={milkHeight} color={MILK}>
                    <Text style={styles.textStyle}>Молоко</Text>
                </Trapezoid>}
                {!!waterHeight && <Trapezoid width={bottomWidth + waterWidth} height={waterHeight} color={WATER}>
                    <Text style={styles.textStyle}>Вода</Text>
                </Trapezoid>}
                {!!coffeeHeight && <Trapezoid width={bottomWidth + coffeeWidth} height={coffeeHeight} color={COFFEE}>
                    <Text style={styles.textStyle}>Кофе</Text>
                </Trapezoid>}
            </View>
        )
    }
}

Cup.defaultProps = {
    coffeeHeight: 0,
    waterHeight: 0,
    milkHeight: 0,
    percent: false,
    style: {}
};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStyle: {
        fontSize: SIZE_H6,
        color: '#fff'
    }
});

export default Cup