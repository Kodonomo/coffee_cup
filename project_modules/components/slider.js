import React from 'react'
import {View, StyleSheet} from 'react-native'
import {coefficientHeight, coefficientWidth} from "../constants/dimensions";
import {Icon} from 'native-base'
import LinearGradient from "react-native-linear-gradient";


class Slider extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                colors={['#b57b99', '#b56d81', '#bb5f6e']}
                                style={[styles.line, {width: this.props.width}]}/>
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                colors={['#b9334e', '#b81b36', '#ce3538']} style={styles.slider}>
                    {this.props.position === 0 &&
                    <View style={{height: 10}}>
                        <Icon name={'arrow-dropup'} style={{color: '#fff'}}/>
                    </View>}
                    {this.props.position < 0 && <Icon name={'arrow-dropup'} style={{color: '#fff'}}/>}
                    {this.props.position >= 0 && <Icon name={'arrow-dropdown'} style={{color: '#fff'}}/>}
                </LinearGradient>
            </View>
        )
    }
}

Slider.defaultProps = {
    width: 0,
    position: 0
};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%'
    },
    line: {
        height: 3,
        backgroundColor: 'red'
    },
    slider: {
        height: coefficientHeight * 13,
        width: coefficientWidth * 50,
        backgroundColor: 'red',
        borderRadius: coefficientWidth * 15,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default Slider