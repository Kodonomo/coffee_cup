import React from 'react'
import {
    StyleSheet,
    PanResponder,
    Animated,
    View
} from "react-native";
import PropTypes from "prop-types";

class Draggable extends React.Component {
    constructor() {
        super();

        this.state = {
            pan: new Animated.ValueXY(),
            _value: {
                x: 0,
                y: 0
            }
        };
        this.array = [
            -10, -5, 0, 5, 10
        ];
    }

    componentWillMount() {
        this.state.pan.addListener((c) => this.state._value = c);

        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (e, gesture) => true,
            onPanResponderMove: Animated.event([
                null, {
                    dx: 0,
                    dy: this.state.pan.y
                }
            ]),
            onPanResponderGrant: (e, gestureState) => {
                this.state.pan.setOffset({x: this.state._value.x, y: this.state._value.y});
            },
            onPanResponderRelease: () => {
                Animated.spring(
                    this.state.pan, // Auto-multiplexed
                    {toValue: {x: 0, y: this.fixingPosition(this.state.pan.y)}},
                ).start();
            },
        });
    }

    render() {
        return (
                <Animated.View
                    {...this.panResponder.panHandlers}
                    style={[this.state.pan.getLayout(), styles.raw, this.props.style]}
                >
                    {this.props.children}
                </Animated.View>
        );
    }

    fixingPosition(v) {
        let min = this.array[0];
        v = Object.values(v)[1];
        this.array.forEach(e => {
            if ((Math.abs(e - v)) < (Math.abs(v - min)))
                min = e;
        });

        this.array = this.array.map(e => e - min);

        this.props.onChange(min);
        return min;
    }
}

Draggable.propTypes = {
    children: PropTypes.element.isRequired
};

Draggable.defaultProps = {
    style: {}
};

let styles = StyleSheet.create({
    raw: {
        width: '100%'
    }
});

export default Draggable