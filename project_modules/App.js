import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet, Text, View, Switch} from 'react-native';
import Slider from "./components/slider";
import {
    coefficientHeight,
    coefficientWidth,
    CUP_SIZE,
    SIZE_H1,
    SIZE_H3,
    SIZE_H4,
    SIZE_H6
} from "./constants/dimensions";
import Cup from "./components/cup";
import Draggable from "./components/draggable";
import LinearGradient from "react-native-linear-gradient";
import {COFFEE, DARK_GRADIENT, LIGHT_GRADIENT, MILK, WATER} from "./constants/colors";
import RoundButton from "./components/round_button";
import {LinearTextGradient} from "react-native-text-gradient";

type Props = {};
export default class App extends Component<Props> {
    constructor(props) {
        super(props);

        this.coffeeHeight = coefficientHeight * 30;
        this.waterHeight = coefficientHeight * 75;
        this.milkHeight = coefficientHeight * 15;

        this.state = {
            milk: false,
            sugar: 0,
            coffeeHeight: this.coffeeHeight,
            waterHeight: this.waterHeight,
            milkHeight: this.milkHeight
        }
    }

    render() {
        let angle = CUP_SIZE["175"];
        let width = coefficientHeight * 280 * angle + 190 * coefficientWidth + 10;
        let sum = this.state.coffeeHeight + this.state.waterHeight + this.state.milkHeight;
        let emptyWidth = (coefficientHeight * 140 - sum) * angle;
        let milkWidth = (this.state.milkHeight) * angle + emptyWidth;
        let waterWidth = (this.state.waterHeight) * angle + milkWidth;
        let coffeeLength = width - coefficientWidth * 50 - waterWidth + 10;
        let waterLength = width - coefficientWidth * 50 - milkWidth + 10;
        let milkLength = width - coefficientWidth * 50 - emptyWidth + 10;

        return (
            <View style={styles.container}>
                <RoundButton style={{position: 'absolute', left: 10, top: 10}}/>
                <View style={styles.title}>
                    <LinearTextGradient
                        locations={[0, 1]}
                        colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
                        start={{x: 0, y: 1}}
                        end={{x: 1, y: 0}}>
                        <Text style={{fontSize: SIZE_H1}}>Американо</Text>
                    </LinearTextGradient>
                    <Text style={{fontSize: SIZE_H3, marginBottom: 10}}>250 мл</Text>
                    <Text style={{textAlign: 'center', fontSize: SIZE_H6}}>Перемещайте ползунки, чтобы указать крепость
                        кофе и количество молока.</Text>
                </View>
                <View style={[styles.cupContainer, {width: width}]}>
                    <Cup coffeeHeight={this.state.coffeeHeight}
                         waterHeight={this.state.waterHeight}
                         milkHeight={this.state.milk ? this.state.milkHeight : 0}
                         style={{
                             position: 'absolute',
                             left: 0,
                             top: 0
                         }}/>
                    <View style={[styles.sliderLine, {bottom: this.coffeeHeight - 15}]}>
                        <Draggable
                            onChange={(value) => {
                                this.setState({
                                    coffeeHeight: this.state.coffeeHeight - value
                                });
                            }}
                            style={styles.draggableStyle}>
                            <Slider width={coffeeLength} position={0}/>
                        </Draggable>
                    </View>

                    <View style={[styles.sliderLine, {bottom: this.state.coffeeHeight + this.waterHeight - 15}]}>
                        <Draggable
                            onChange={(value) => {
                                this.setState({
                                    waterHeight: this.state.waterHeight - value
                                });
                            }}
                            style={styles.draggableStyle}>
                            <Slider width={waterLength} position={0}/>
                        </Draggable>
                    </View>
                    {this.state.milk && <View
                        style={[styles.sliderLine, {bottom: this.state.coffeeHeight + this.state.waterHeight + this.milkHeight - 15}]}>
                        <Draggable
                            onChange={(value) => {
                                this.setState({
                                    milkHeight: this.state.milkHeight - value
                                });
                            }}
                            style={styles.draggableStyle}>
                            <Slider width={milkLength} position={0}/>
                        </Draggable>
                    </View>}
                </View>

                <View style={{marginTop: 10, marginBottom: 10}}>
                    <View style={{flexDirection: 'row', width: '100%'}}>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
                            <RoundButton name={'remove'} style={{marginRight: 10}}
                                         press={() => this.setState({sugar: this.state.sugar - 1})}/>
                            <LinearTextGradient
                                locations={[0, 1]}
                                colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
                                start={{x: 0, y: 1}}
                                end={{x: 1, y: 0}}>
                                <Text style={{fontSize: SIZE_H3}}>{this.state.sugar || 0}</Text>
                            </LinearTextGradient>
                            <RoundButton name={'add'} style={{marginLeft: 10}}
                                         press={() => this.setState({sugar: this.state.sugar + 1})}/>
                        </View>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
                            <Switch
                                trackColor={{false: WATER, true: MILK}}
                                value={this.state.milk}
                                thumbColor={COFFEE}
                                onValueChange={(value) => this.setState({milk: value})}/>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <Text>Сахар</Text>
                        </View>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <Text>Молоко</Text>
                        </View>
                    </View>
                </View>
                <TouchableOpacity onPress={() => 1}>
                    <LinearGradient colors={[DARK_GRADIENT, LIGHT_GRADIENT]} style={styles.bigButton}
                                    start={{x: 0, y: 1}} end={{x: 1, y: 0}}>
                        <Text style={{fontSize: SIZE_H4, color: '#fff'}}>Оплатить</Text>
                        <View style={styles.innerButton}>
                            <Text style={{fontWeight: 'bold', fontSize: SIZE_H4, color: DARK_GRADIENT}}>30₽</Text>
                        </View>
                    </LinearGradient>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    title: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
        marginBottom: 15
    },
    cupContainer: {
        alignItems: 'center',
        height: coefficientHeight * 150,
    },
    draggableStyle: {
        position: 'absolute'
    },
    sliderLine: {
        width: '100%',
        height: 30,
        position: 'absolute'
    },
    bigButton: {
        width: coefficientWidth * 230,
        height: coefficientHeight * 25,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: coefficientHeight * 25,
    },
    innerButton: {
        position: 'absolute',
        left: 0,
        top: -2,
        margin: 8,
        width: coefficientWidth * 60,
        height: coefficientHeight * 20,
        backgroundColor: '#fff',
        borderRadius: coefficientHeight * 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

/*let dash = [];
        const a = (id) => {
            if (id % 2 === 0) {
                return {
                    backgroundColor: '#ffffff' + ((4 - id / 2) * 256).toString(16)
                }
            } else {
                return {
                    backgroundColor: '#ffffff00'
                }
            }
        };
        for (let i = 0; i < 8; i++) {
            dash.push(
                <View
                    key={i}
                    style={[a(i), {height: 10, width: 20}]}
                >
                </View>
            )
        }

 */
